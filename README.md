![](https://img.shields.io/badge/Microverse-blueviolet)

# Project Name
CATALOG APP

> Description the project.
A console app that helps one to keep track of the own things they own such as movie, music albums, games, books


## Built With

- Ruby
- RSpec
- Technologies used

## Live Demo (if available)

[Live Demo Link](https://livedemo.com)


## Getting Started

**This is an example of how you may give instructions on setting up your project locally.**
**Modify this file to match your project, remove sections that don't apply. For example: delete the testing section if the currect project doesn't require testing.**


To get a local copy up and running follow these simple example steps.

### Prerequisites
Basic knowlwedge of computers, git and an editor most prefarably visual studio code

### Setup
Navigate to the directory in your local computer where you want this project to be.
Once inside open the terminal in that directory and clone the project
Run git clone(https://github.com/AbbyNyakara/catalog-app.git)

### Install
When the clone is complete change the current working directory to the catalog app
Run cd catalog-app
Then open it VsCode by running ==> code .
### Usage

### Run tests

### Deployment
Once you create the file for the tests with the _spec.rb extension, jusr run rspec to run the tests


## Authors

👤 **Abby**

- GitHub: [@AbbyNyakara](https://github.com/AbbyNyakara)
- Twitter: [@AbigaelNyakara](https://twitter.com/AbbyNyakara)
- LinkedIn: [Abigael Nyakara](https://linkedin.com/in/AbbyNyakara)


👤 **Elisha Tetteyfio**

- GitHub: [@elisha-tetteyfio](https://github.com/elisha-tetteyfio)
- Twitter: [@twitterhandle](https://twitter.com/Nii_AlYasa)
- LinkedIn: [LinkedIn](https://linkedin.com/in/elisha-tetteyfio)

👤 **Alphonce**
- Github: [@tingamapuro04](https://github.com/tingamapuro04)
- LinkedIn: [@adoyo-alphonce](https://www.linkedin.com/in/adoyo-alphonce/)

👤 **Afolabi Akorede**

- GitHub: [@brainconnect93](https://github.com/brainconnect93)
- Twitter: [@brainconnect0](https://twitter.com/brainconnect0)
- LinkedIn: [Afolabi Akorede](https://linkedin.com/in/brainconnect93)


## 🤝 Contributing

Contributions, issues, and feature requests are welcome!

Feel free to check the [issues page](../../issues/).

## Show your support

Give a ⭐️ if you like this project!

## Acknowledgments

- Hat tip to anyone whose code was used
- Inspiration
- etc

## 📝 License

This project is [MIT](./MIT.md) licensed.
